/**
 * This package defines a concrete implementation of 
 * {@link com.nada.slf4j.JsonLoggingEventBuilder} which is based on google
 * JSON serialization class: {@link com.google.gson.Gson} 
 */
package com.nada.slf4j.gson;