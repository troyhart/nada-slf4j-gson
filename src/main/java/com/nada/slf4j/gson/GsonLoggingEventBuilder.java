package com.nada.slf4j.gson;


import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.nada.slf4j.JsonLoggingEventBuilder;

/**
 * @author troy.hart@gmail.com
 */
public class GsonLoggingEventBuilder
  implements JsonLoggingEventBuilder
{
  @SuppressWarnings("unused")
  private String timestamp;

  @SuppressWarnings("unused")
  private String level;

  @SuppressWarnings("unused")
  private String message;

  @SuppressWarnings("unused")
  private String sourceId;

  @SuppressWarnings("unused")
  private String hostName;

  @SuppressWarnings("unused")
  private String logger;

  @SuppressWarnings("unused")
  private String thread;

  @SuppressWarnings("unused")
  private String stacktrace;

  @SuppressWarnings("unused")
  private Map<String, String> mdc;

  @SuppressWarnings("unused")
  private Map<Integer, Map<String, String>> diagnostics;

  @SuppressWarnings("unused")
  private List<String> markers;

  @Override
  public void setApplicationName(String appName)
  {
    this.sourceId = appName;
  }

  @Override
  public void setApplicationHostName(String appHostName)
  {
    this.hostName = appHostName;
  }

  @Override
  public void setTimestamp(String timestamp)
  {
    this.timestamp = timestamp;
  }

  @Override
  public void setMessage(String message)
  {
    this.message = message;
  }

  @Override
  public void setLevel(String level)
  {
    this.level = level;
  }

  @Override
  public void setLogger(String logger)
  {
    this.logger = logger;
  }

  @Override
  public void setThread(String thread)
  {
    this.thread = thread;
  }

  @Override
  public void setStacktrace(String stacktrace)
  {
    this.stacktrace = stacktrace;
  }

  @Override
  public void setMDC(Map<String, String> mdc)
  {
    this.mdc = mdc;
  }

  @Override
  public void setDiagnostics(Map<Integer, Map<String, String>> diagnostics)
  {
    this.diagnostics = diagnostics;
  }

  @Override
  public void setMarkers(List<String> markers)
  {
    this.markers = markers;
  }

  @Override
  public String toJson()
  {
    return new Gson().toJson(this);
    //return new GsonBuilder().setPrettyPrinting().create().toJson(this);
  }
}
